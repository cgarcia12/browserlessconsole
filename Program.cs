﻿// ***********************************************************************
// Assembly         : BrowserlessConsole
// Author           : cgarcia
// Created          : 01-28-2020
//
// Last Modified By : cgarcia
// Last Modified On : 01-28-2020
// ***********************************************************************
// <copyright file="Program.cs" company="BrowserlessConsole">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;

/// <summary>
/// The BrowserlessConsole namespace.
/// </summary>
namespace BrowserlessConsole
{
    /// <summary>
    /// Class Program.
    /// </summary>
    internal class Program
    {
        private static readonly Uri _endpoint = new Uri("http://shlbrowse01.tveyes.com/webdriver");
        
        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <link># https://docs.browserless.io/docs/dotnet.html</link>
        public static void Main(string[] args)
        {
            // Note we set our token here, with `true` as a third arg
            //options.AddAdditionalCapability("browserless.token", "YOUR-API-TOKEN", true);

            string script = File.ReadAllText("yt-proxy.js");

            var news = ProcessNewsFeed(script);
            Console.WriteLine(news.Item1.Count);

            var trending = ProcessTrendingFeed(script);
            Console.WriteLine(trending.Item1.Count);

            Console.WriteLine("Press any key to exit");
            Console.ReadLine();
        }

        /// <summary>
        /// Processes the trending feed.
        /// </summary>
        /// <param name="script">The script.</param>
        /// <returns>Tuple&lt;List&lt;Video&gt;, List&lt;Channel&gt;&gt;.</returns>
        private static Tuple<List<Video>, List<Channel>> ProcessTrendingFeed(string script)
        {
            List<Video> videos;
            List<Channel> channels;
            IWebDriver driver = new RemoteWebDriver(_endpoint,
                                                    GetCapabilities());
            using(driver)
            {
                driver.Navigate().GoToUrl("https://www.youtube.com/feed/trending");
                var js = (IJavaScriptExecutor)driver;
                js.ExecuteScript(script);
                string videoJson = js.ExecuteScript("return window.extractTrendingVideos()")
                                     .ToString();
                videos = JsonConvert.DeserializeObject<List<Video>>(videoJson);
                Console.WriteLine(videos.Count);

                string channelJson = js.ExecuteScript("return window.extractChannels()").ToString();
                channels = JsonConvert.DeserializeObject<List<Channel>>(channelJson);
                Console.WriteLine(channels.Count);

                // # Always call `quit` to ensure your session cleans up
                // # properly and you're not charged for unused time
                driver.Quit();
            }

            return new Tuple<List<Video>, List<Channel>>(videos, channels);
        }

        /// <summary>
        /// Processes the news feed.
        /// </summary>
        /// <param name="script">The script.</param>
        /// <returns>Tuple&lt;List&lt;Video&gt;, List&lt;Channel&gt;&gt;.</returns>
        private static Tuple<List<Video>, List<Channel>> ProcessNewsFeed(string script)
        {
            List<Video> videos;
            List<Channel> channels;
            IWebDriver driver = new RemoteWebDriver(_endpoint,
                                                    GetCapabilities());
            using(driver)
            {
                driver.Navigate().GoToUrl("https://www.youtube.com/channel/UCYfdidRxbB8Qhf0Nx7ioOYw");
                var js = (IJavaScriptExecutor)driver;
                js.ExecuteScript(script);
                string videoJson = js.ExecuteScript("return window.extractNewsVideos()")
                                     .ToString();
                videos = JsonConvert.DeserializeObject<List<Video>>(videoJson);
                Console.WriteLine(videos.Count);

                string channelJson = js.ExecuteScript("return window.extractChannels()").ToString();
                channels = JsonConvert.DeserializeObject<List<Channel>>(channelJson);
                Console.WriteLine(channels.Count);

                // # Always call `quit` to ensure your session cleans up
                // # properly and you're not charged for unused time
                driver.Quit();
            }

            return new Tuple<List<Video>, List<Channel>>(videos, channels);
        }

        /// <summary>
        /// Gets the capabilities.
        /// </summary>
        /// <returns>ICapabilities.</returns>
        private static ICapabilities GetCapabilities()
        {
            ChromeOptions options = new ChromeOptions();

            // Set launch args similar to puppeteer's for best performance
            options.AddArgument("--disable-background-timer-throttling");
            options.AddArgument("--disable-backgrounding-occluded-windows");
            options.AddArgument("--disable-breakpad");
            options.AddArgument("--disable-component-extensions-with-background-pages");
            options.AddArgument("--disable-dev-shm-usage");
            options.AddArgument("--disable-extensions");
            options.AddArgument("--disable-features=TranslateUI,BlinkGenPropertyTrees");
            options.AddArgument("--disable-ipc-flooding-protection");
            options.AddArgument("--disable-renderer-backgrounding");
            options.AddArgument("--enable-features=NetworkService,NetworkServiceInProcess");
            options.AddArgument("--force-color-profile=srgb");
            options.AddArgument("--hide-scrollbars");
            options.AddArgument("--metrics-recording-only");
            options.AddArgument("--mute-audio");
            options.AddArgument("--headless");
            options.AddArgument("--no-sandbox");
            return options.ToCapabilities();
        }
    }

    /// <summary>
    /// Class Video. This class cannot be inherited.
    /// </summary>
    [JsonObject]
    public sealed class Video
    {
        /// <summary>
        /// Gets or sets the link.
        /// </summary>
        /// <value>The link.</value>
        [JsonProperty("link")]
        public string Link { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        [JsonProperty("title")]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the owner.
        /// </summary>
        /// <value>The owner.</value>
        [JsonProperty("channel")]
        public Channel Owner { get; set; }
    }

    /// <summary>
    /// Class Channel. This class cannot be inherited.
    /// </summary>
    [JsonObject]
    public sealed class Channel
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the link.
        /// </summary>
        /// <value>The link.</value>
        [JsonProperty("link")]
        public string Link { get; set; }
    }
}
