﻿var __proxy = {}

__proxy.getVideos = function () {
	const videos = document.querySelectorAll('ytd-rich-grid-video-renderer');
	const results = [];
	for (let t = 0; t < videos.length; t++) {
		const link = videos[t].querySelector('a#video-title-link');
		const title = link['title'];
		const href = link['href'];
		const video = {
			title: title,
			link: href
		};
		results.push(video);
	}
	return JSON.stringify(results);
};

__proxy.getNewsVideos = function() {
	const videos = document.querySelectorAll('ytd-grid-video-renderer');
	const results = [];
	for (let t = 0; t < videos.length; t++) {
		const link = videos[t].querySelector('a#video-title');
		const title = link['title'];
		const href = link['href'];
		const channelLink = videos[t].querySelector('ytd-channel-name a')['href'];
		const channelName = videos[t].querySelector('yt-formatted-string')['title'];
		const vid = {
			title: title,
			link: href,
			channel: { link: channelLink,name:channelName}
		};
		results.push(vid);
	}
	return JSON.stringify(results);
};

__proxy.getTrendingVideos = function() {
	const videos = document.querySelectorAll('ytd-video-renderer');
	const results = [];
	for (let t = 0; t < videos.length; t++) {
		const link = videos[t].querySelector('a#video-title');
		const title = link['title'];
		const href = link['href'];
		const channelLink = videos[t].querySelector('ytd-channel-name a')['href'];
		const channelOwner = videos[t].querySelector('yt-formatted-string a').innerText;
		const vid = {
			title: title,
			link: href,
			channel: { name: channelOwner, link: channelLink }
		};
		results.push(vid);
	}
	return JSON.stringify(results);
};

__proxy.getChannels = function () {
	const result = [];
	const channels = document.querySelectorAll('ytd-channel-name a');
	for (let i = 0; i < channels.length; i++) {
		const channelName = channels[i].innerText;
		const channelHref = channels[i]['href'];
		const channel = {
			name: channelName,
			link: channelHref
		};
		result.push(channel);
	}
	return JSON.stringify(result);
};

__proxy.scrollToBottom = function () {
	window.scrollTo(0, document.body.scrollHeight);
};


window.extractVideos = function() {
	return __proxy.getVideos();
};

window.extractNewsVideos = function() {
	return __proxy.getNewsVideos();
};

window.extractTrendingVideos = function () {
	return __proxy.getTrendingVideos();
};

window.extractChannels = function() {
	return __proxy.getChannels();
};

window.execScroll = function() {
	__proxy.scrollToBottom();
}